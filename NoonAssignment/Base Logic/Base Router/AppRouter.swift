//
//  AppRouter.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

protocol AppRouter {
    var viewController : UIViewController? {get set}
}
