//
//  StoryBoardInit.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

protocol StoryboardableInitProtocol {
    //ViewModel
    associatedtype ViewModelType
    associatedtype RouterType
    var viewModel: ViewModelType! { get set }
    var router: RouterType! { get set }
    //Methods
    static func instantiate(with viewModel: Self.ViewModelType,
                            router: Self.RouterType,
                            storyBoardName: String) -> Self
}

extension StoryboardableInitProtocol where Self: UIViewController {
    
    static func instantiate(with viewModel: Self.ViewModelType,
                            router: Self.RouterType,
                            storyBoardName: String) -> Self {
        let storyboardId = String(describing: self)
        let viewController = UIStoryboard(name: storyBoardName,
                                          bundle: nil).instantiateViewController(withIdentifier: storyboardId)
        guard var vc = viewController as? Self else {
            fatalError("Failed to instantiate '\(storyboardId)'")
        }
        vc.viewModel = viewModel
        vc.router = router
        return vc
    }
}
