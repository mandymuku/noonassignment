//
//  EmployeeDetailRouter.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

protocol EmployeeDetailRouterProtocol: AppRouter {
    func showSuccessAlert()
    func showErrorAlert(_ error: String)
}

class EmployeeDetailRouter: EmployeeDetailRouterProtocol {
    var viewController: UIViewController?
}

extension EmployeeDetailRouter {
    func showErrorAlert(_ error: String) {
        guard let vc = viewController else {
            return
        }
        vc.showAlertWith(msg: error)
    }
    
    func showSuccessAlert() {
        guard let vc = viewController else {
            return
        }
        let alertVc = UIAlertController(title: "NoonAssignment",
                                        message: "Employee updated successfully",
                                        preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK",
                                        style: .default) { _ in
                                            vc.navigationController?.popToRootViewController(animated: true)
        }
        alertVc.addAction(alertAction)
        vc.present(alertVc, animated: true,
                   completion: nil)
    }
}
