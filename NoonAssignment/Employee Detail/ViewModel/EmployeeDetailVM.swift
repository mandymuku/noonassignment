//
//  EmployeeDetailVM.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

protocol EmployeeDetailVMProtocol {
    func getEmployee() -> Employee
    func updateEmployee(_ model: EmployeeFormModelProtocol)
}

class EmployeeDetailVM: NSObject, EmployeeDetailVMProtocol {
    
    //MARK: - Variables
    private var employee: Employee
    
    //Init
    init(employee: Employee) {
        self.employee = employee
    }
    
    func getEmployee() -> Employee {
        return employee
    }
    
    func updateEmployee(_ model: EmployeeFormModelProtocol) {
        CoreDataUtils.shared.updateEmployee(employee,
                                            updatedData: model)
    }
}
