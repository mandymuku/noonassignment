//
//  StringEx.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

extension String {
    func toDate(format: String) -> Date? {
        return DateFormatter(format: format).date(from: self)
    }
}


extension String {
    func checkEmpty() -> String? {
        let str = self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if str.isEmpty {
            return nil
        }
        return str
    }
}
