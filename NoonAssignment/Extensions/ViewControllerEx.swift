
//
//  ViewControllerEx.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlertWith(title: String = "NoonAssignment",
                       msg : String,
                       butTitle: String = "OK"){
        let alertVc = UIAlertController(title: title,
                                        message: msg,
                                        preferredStyle: .alert)
        let alertAction = UIAlertAction(title: butTitle,
                                        style: .default) { (action) in
            self.dismiss(animated: true,
                         completion: nil)
        }
        alertVc.addAction(alertAction)
        self.present(alertVc, animated: true,
                     completion: nil)
    }
}
