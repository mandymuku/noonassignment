//
//  TextFieldEx.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

extension UITextField {
    func checkNilTextfield() -> String? {
        guard let textTemp = self.text else {
            return nil
        }
        return textTemp
    }
}
