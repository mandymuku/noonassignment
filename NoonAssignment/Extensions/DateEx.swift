//
//  DateEx.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

extension DateFormatter {
    convenience init(format: String) {
        self.init()
        dateFormat = format
        locale = Locale.current
    }
}

extension Date {
    func toString(format: String) -> String? {
        return DateFormatter(format: format).string(from: self)
    }
}
