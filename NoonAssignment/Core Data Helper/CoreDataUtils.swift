//
//  CoreDataUtils.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import CoreData
import UIKit

class CoreDataUtils: NSObject {
    static let shared = CoreDataUtils()

    //Save
    func createEmployee(name: String,
                      email: String,
                      city: String,
                      isMarried: Bool,
                      anniversaryDate: Date? = nil) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }

        // 1: Get context & Set values
        let managedContext = appDelegate.persistentContainer.viewContext

        // 2: Create Entity
        let employee = Employee(context: managedContext)
        employee.name = name
        employee.emailId = email
        employee.city = city
        employee.isMarried = isMarried
        employee.anniversaryDate = anniversaryDate

        // 4: Save
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    //Update
    func updateEmployee(_ manageObjectOfEmployee: Employee,
                        updatedData: EmployeeFormModelProtocol) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        // 1: Get context & Set values
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // 2: Updated Entity
        manageObjectOfEmployee.name = updatedData.name
        manageObjectOfEmployee.emailId = updatedData.email
        manageObjectOfEmployee.city = updatedData.city
        manageObjectOfEmployee.isMarried = updatedData.isMarried
        manageObjectOfEmployee.anniversaryDate = updatedData.annivarsaryDate
        
        // 4: Save
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }

    }
    
    //Delete
    func deleteEmployee(_ manageObjectOfEmployee: Employee) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        // 1: Get context & Set values
        let managedContext = appDelegate.persistentContainer.viewContext
       
        // 2: Delete
        managedContext.delete(manageObjectOfEmployee)
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}
