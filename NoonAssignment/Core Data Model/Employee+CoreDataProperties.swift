//
//  Employee+CoreDataProperties.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//
//

import Foundation
import CoreData


extension Employee {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<Employee> {
        return NSFetchRequest<Employee>(entityName: "Employee")
    }

    @NSManaged public var name: String?
    @NSManaged public var city: String?
    @NSManaged public var isMarried: Bool
    @NSManaged public var emailId: String?
    @NSManaged public var anniversaryDate: Date?

}
