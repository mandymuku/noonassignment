//
//  EmployeeCreateVM.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

protocol EmployeeCreateVMProtocol {
    func createNewEmployeeEntry(_ model: EmployeeFormModelProtocol)
}

class EmployeeCreateVM: NSObject, EmployeeCreateVMProtocol {

    func createNewEmployeeEntry(_ model: EmployeeFormModelProtocol) {
        CoreDataUtils.shared.createEmployee(name: model.name,
                                          email: model.email,
                                          city: model.city,
                                          isMarried: model.isMarried,
                                          anniversaryDate: model.annivarsaryDate)
    }
}
