//
//  EmployeeCreateVC.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

final class EmployeeCreateVC: UIViewController, StoryboardableInitProtocol {
    
    //MARK: - Variables
    var viewModel: EmployeeCreateVMProtocol!
    var router: EmployeeCreateRouterProtocol!
    
    lazy var formView: EmployeeFormView = {
       
        let _formView = UINib(nibName: "EmployeeFormView",
                              bundle: nil).instantiate(withOwner: self,
                                                       options: nil).first as! EmployeeFormView
        _formView.viewModel = EmployeeFormVM()
        _formView.frame = view.bounds
        return _formView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Create Employee"
        view.addSubview(formView)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(tappedSaveBarButton))
    }
    
    @objc func tappedSaveBarButton() {
        let model = formView.validateTextFields()
        if let error = model.error {
            router.showErrorAlert(error)
        } else if let employee = model.employee {
            viewModel.createNewEmployeeEntry(employee)
            router.showSuccessAlert()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
