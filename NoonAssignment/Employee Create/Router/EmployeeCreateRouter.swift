//
//  EmployeeCreateRouter.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

protocol EmployeeCreateRouterProtocol: AppRouter {
    func showSuccessAlert()
    func showErrorAlert(_ error: String)
}

class EmployeeCreateRouter: EmployeeCreateRouterProtocol {
    var viewController: UIViewController?
}

extension EmployeeCreateRouter {
    func showErrorAlert(_ error: String) {
        guard let vc = viewController else {
            return
        }
        vc.showAlertWith(msg: error)
    }

    func showSuccessAlert() {
        guard let vc = viewController else {
            return
        }
        let alertVc = UIAlertController(title: "NoonAssignment",
                                        message: "Employee added successfully",
                                        preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK",
                                        style: .default) { _ in
            vc.navigationController?.popToRootViewController(animated: true)
        }
        alertVc.addAction(alertAction)
        vc.present(alertVc, animated: true,
                   completion: nil)
    }
}
