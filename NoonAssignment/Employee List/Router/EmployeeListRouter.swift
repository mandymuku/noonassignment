//
//  EmployeeListRouter.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

protocol EmployeeListRouterProtocol: AppRouter {
    func routeToDetailOfEmployee(_ employee: Employee)
    func routeToCreateEmployee()
    func showNoResultsAlert()
}

class EmployeeListRouter: EmployeeListRouterProtocol {
    var viewController: UIViewController?
}

extension EmployeeListRouter {
    func routeToDetailOfEmployee(_ employee: Employee) {
        guard let vc = viewController else {
            return
        }
        //Viewmodel
        let viewModel = EmployeeDetailVM(employee: employee)
        //Router
        let employeeDetailRouter = EmployeeDetailRouter()
        
        //Storyboard init
        let employeeDetailVC = EmployeeDetailVC.instantiate(with: viewModel,
                                                    router: employeeDetailRouter,
                                                    storyBoardName: "Main")
        employeeDetailRouter.viewController = employeeDetailVC
        vc.navigationController?.pushViewController(employeeDetailVC,
                                                    animated: true)
    }
    
    func routeToCreateEmployee() {
        guard let vc = viewController else {
            return
        }
        //Viewmodel
        let viewModel = EmployeeCreateVM()
        //Router
        let employeeCreateRouter = EmployeeCreateRouter()
        
        //Storyboard init
        let employeeCreateVC = EmployeeCreateVC.instantiate(with: viewModel,
                                                            router: employeeCreateRouter,
                                                            storyBoardName: "Main")
        employeeCreateRouter.viewController = employeeCreateVC
        vc.navigationController?.pushViewController(employeeCreateVC,
                                                    animated: true)
    }
    
    func showNoResultsAlert() {
        guard let vc = viewController else {
            return
        }
        vc.showAlertWith(msg: "No Results")
    }
}
