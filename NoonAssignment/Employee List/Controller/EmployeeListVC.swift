//
//  EmployeeListVC.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import CoreData
import UIKit


final class EmployeeListVC: UIViewController, StoryboardableInitProtocol {
    
    // MARK: - Variables
    var viewModel: EmployeeListVMProtocol!
    var router: EmployeeListRouterProtocol!
    
    fileprivate lazy var fetchedResultsController: NSFetchedResultsController<Employee> = {
        // Create Fetch Request
        let fetchRequest: NSFetchRequest<Employee> = Employee.createFetchRequest()
        // Configure Fetch Request
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name",
                                                         ascending: true)]
        // Create Fetched Results Controller
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        // Get context
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: managedContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)
        // Configure Fetched Results Controller
        fetchedResultsController.delegate = self
        return fetchedResultsController
    }()
    
    // MARK: - IBOutlets

    @IBOutlet var tableView: UITableView! {
        didSet {
            tableView.tableFooterView = UIView(frame: CGRect.zero)
            tableView.register(UINib(nibName: EmployeeTableViewCell.reuseIdentifier(),
                                     bundle: nil),
                               forCellReuseIdentifier: EmployeeTableViewCell.reuseIdentifier())
            tableView.delegate = self
            tableView.dataSource = self
        }
    }



    // MARL: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Employee List"
        // Do any additional setup after loading the view.
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                            target: self,
                                                            action: #selector(tappedAddBarButton))
        fetchAllEmployee()
    }

    // MARK: - Helper and Action Methods

    func fetchAllEmployee(){
        do{
            /*initiate performFetch() call on fetchedResultsController*/
            try fetchedResultsController.performFetch()
            
        }catch{
            print(error)
        }
    }
    
    @objc func tappedAddBarButton() {
        router.routeToCreateEmployee()
    }

    /*
     // MARK: - Navigation

     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
}

extension EmployeeListVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else {
            return 0
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EmployeeTableViewCell.reuseIdentifier()) as! EmployeeTableViewCell
        let model = fetchedResultsController.object(at: indexPath)
        cell.configureEmployeeCell(model)
        return cell
    }
}

extension EmployeeListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = fetchedResultsController.object(at: indexPath)
        router.routeToDetailOfEmployee(model)
    }
}

extension EmployeeListVC: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>,
                    didChange anObject: Any,
                    at indexPath: IndexPath?,
                    for type: NSFetchedResultsChangeType,
                    newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath,
                let cell = tableView.cellForRow(at: indexPath) as? EmployeeTableViewCell {
                let model = fetchedResultsController.object(at: indexPath)
                cell.configureEmployeeCell(model)
            }
        default:
            break
        }
    }
}
