//
//  EmployeeFormView.swift
//  NoonAssignment
//
//  Created by mukesh on 5/8/19.
//  Copyright © 2019 NoonAcademy. All rights reserved.
//

import UIKit

fileprivate let dateFormat: String = "yyyy-MM-dd"

protocol EmployeeFormModelProtocol {
    var name: String { get set }
    var email: String { get set }
    var city: String { get set }
    var isMarried: Bool { get set }
    var annivarsaryDate: Date? { get set }
}

struct EmployeeFormModel: EmployeeFormModelProtocol {
    var name: String
    var email: String
    var city: String
    var isMarried: Bool
    var annivarsaryDate: Date?
}

class EmployeeFormView: UIView {
    // MARK: - Outlets

    @IBOutlet var txtFieldName: UITextField! {
        didSet {
            txtFieldName.delegate = self
        }
    }

    @IBOutlet var txtFieldEmail: UITextField! {
        didSet {
            txtFieldEmail.delegate = self
        }
    }

    @IBOutlet var txtFieldCity: UITextField! {
        didSet {
            txtFieldCity.delegate = self
        }
    }

    @IBOutlet var txtFieldMarriage: UITextField! {
        didSet {
            txtFieldMarriage.delegate = self
        }
    }

    @IBOutlet var switchMarriage: UISwitch! {
        didSet {
            switchMarriage.addTarget(self,
                                     action: #selector(tappedMarriageSwitch(sender:)),
                                     for: .valueChanged)
        }
    }

    @IBOutlet var datePickerAnniversary: UIDatePicker! {
        didSet {
            datePickerAnniversary.maximumDate = Date()
            datePickerAnniversary.addTarget(self,
                                            action: #selector(pickedAnniversaryDate(sender:)),
                                            for: .valueChanged)
        }
    }

    @IBOutlet var pickerCity: UIPickerView! {
        didSet {
            pickerCity.delegate = self
            pickerCity.dataSource = self
        }
    }

    @IBOutlet var butDone: UIButton!

    // MARK: - Variables

    var viewModel: EmployeeFormVMProtocol?
    private let cityArr: [String] = ["Delhi", "Bengaluru", "Hyderabad", "Mumbai", "Pune", "Kolkata"]

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - Helper Methods

    func configureWithEmployee(_ model: Employee) {
        txtFieldName.text = model.name
        txtFieldEmail.text = model.emailId
        txtFieldCity.text = model.city
        if model.isMarried {
            switchMarriage.setOn(true, animated: false)
            txtFieldMarriage.isEnabled = true
            txtFieldMarriage.placeholder = "Enter Anniversary Date"
            txtFieldMarriage.text = model.anniversaryDate?.toString(format: dateFormat)
        }
    }

    @objc func tappedMarriageSwitch(sender: UISwitch) {
        
        //Date Picker State manage w.r.t to switch
        datePickerAnniversary.isHidden = !sender.isOn
        butDone.isHidden = !sender.isOn
        txtFieldMarriage.isEnabled = sender.isOn
        txtFieldMarriage.placeholder = sender.isOn ? "Enter Anniversary Date" : ""
        txtFieldMarriage.text = sender.isOn ? datePickerAnniversary.maximumDate?.toString(format: "yyyy-MM-dd") : ""
        
        //Remove Keyboard
        hideKeyboard()
        
        // City Picker State : Hidden
        toggleCityPicker(true)
    }

    @IBAction func tappedDoneBut(_ sender: Any) {
        toggleCityPicker(true)
        toggleDatePicker(true)
    }
    

    func showCityPicker() {
        //Hide Date Picker
        toggleDatePicker(true)
        //Show City Picker
        toggleCityPicker(false)
        //Set it to first object
        txtFieldCity.text = cityArr[0]
    }

    @objc func pickedAnniversaryDate(sender: UIDatePicker) {
        if let date = sender.date.toString(format: dateFormat) {
            txtFieldMarriage.text = date
        }
    }
    
    func hideKeyboard() {
        self.endEditing(true)
    }
}

//Toggle Pickers
extension EmployeeFormView {
    func toggleCityPicker(_ bool: Bool) {
        pickerCity.isHidden = bool
        butDone.isHidden = bool
    }
    
    
    func toggleDatePicker(_ bool: Bool) {
        datePickerAnniversary.isHidden = bool
        butDone.isHidden = bool
    }
}
// Validations
extension EmployeeFormView {
    func validateTextFields() -> (employee: EmployeeFormModel?, error: String?) {
        guard let nameText = txtFieldName.checkNilTextfield(),
            let name = viewModel?.validateName(nameText) else {
            return (employee: nil, error: "Enter valid name")
        }

        guard let emailText = txtFieldEmail.checkNilTextfield(),
            let email = viewModel?.validateEmail(emailText) else {
            return (employee: nil, error: "Enter valid email")
        }

        guard let cityText = txtFieldCity.checkNilTextfield(),
            let city = viewModel?.validateCity(cityText) else {
            return (employee: nil, error: "Enter valid city")
        }

        if !switchMarriage.isOn {
            let model = EmployeeFormModel(name: name,
                                          email: email,
                                          city: city,
                                          isMarried: switchMarriage.isOn,
                                          annivarsaryDate: nil)
            return (employee: model, error: nil)
        }

        guard let anniversaryText = txtFieldMarriage.checkNilTextfield(),
            let anniversary = viewModel?.validateAnniversary(anniversaryText) else {
            return (employee: nil, error: "Enter valid annoiversary date")
        }

        let model = EmployeeFormModel(name: name,
                                      email: email,
                                      city: city,
                                      isMarried: switchMarriage.isOn,
                                      annivarsaryDate: anniversary.toDate(format: dateFormat))
        return (employee: model, error: nil)
    }
}

extension EmployeeFormView: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return cityArr.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cityArr[row]
    }
}

extension EmployeeFormView: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        txtFieldCity.text = cityArr[row]
    }
}

extension EmployeeFormView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case txtFieldCity:
            showCityPicker()
            hideKeyboard()
            return false
        case txtFieldMarriage:
            hideKeyboard()
            return false
        default:
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        hideKeyboard()
        return true
    }
}

protocol EmployeeFormVMProtocol {
    func validateName(_ text: String) -> String?
    func validateEmail(_ text: String) -> String?
    func validateCity(_ text: String) -> String?
    func validateAnniversary(_ text: String) -> String?
}

class EmployeeFormVM: NSObject, EmployeeFormVMProtocol {
    func validateName(_ text: String) -> String? {
        guard let validText = text.checkEmpty(),
            validText.count > 2 else {
            return nil
        }
        return validText
    }

    func validateEmail(_ text: String) -> String? {
        guard let validText = text.checkEmpty(),
            isValidEmail(validText) else {
            return nil
        }
        return validText
    }

    func validateCity(_ text: String) -> String? {
        guard let validText = text.checkEmpty() else {
            return nil
        }
        return validText
    }

    func validateAnniversary(_ text: String) -> String? {
        guard let validText = text.checkEmpty() else {
            return nil
        }
        return validText
    }
}

// Regex Methods
extension EmployeeFormVM {
    func isValidEmail(_ email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}
